# system76-driver-bookworm

    Philip McGrath <philip@philipmcgrath.com>
    2023-05-17

This is a build for Debian Bookworm of the System76 [PPA]. It is a hack and
comes **as-is** with **no warranty**: I’ve worked on it just barely enough to
get control of the fan on a Thelio Mira r1.

The packages were built from the [PPA] basically by following these [build
instructions]. The builds ran in a `podman` container using the
`docker.io/library/debian:bookworm-20230502` image.

The package sources came from the `lunar` suite of the PPA, except that
`firmware-manager` was built from `kinetic`. Apparently `lunar` has `rustc`
version `1.67.1`, while `bookworm` has `1.63.0`, and some dependencies of
`firmware-manager` in the `lunar` suite require `rustc` version `1.65.0` or
greater. At release, `kinetic` had `rustc` version `1.61.0`, but it seems
Ubuntu has put version `1.65.0` in the `*-updates` and `*-security`
distributions all the way back to `focal`. I suspect something significant may
have happened in the `1.65.0` release: perhaps it will also come in an update
after `bookworm` is released as `stable`.

I used [`aptly`] on Kubuntu 22.10 to generate and sign the APT repository.
I've tried to follow the [repository guidelines] reasonably closely, but I
have not packaged the GPG key as recommended under “Certificate rollover and
updates”: I am interested in collaborating on a maintainable solution for
these packages, but this build is emphatically a one-off.

## Usage

To use these packages with APT, unpack this directory as
`/opt/system76-driver-bookworm`. From that directory, create these symlinks
(with `sudo` or as root):

```
ln -sr system76-driver-bookworm-archive-keyring.pgp /etc/apt/keyrings/
ln -sr system76-driver-bookworm.pref                /etc/apt/preferences.d/
ln -sr system76-driver-bookworm.sources             /etc/apt/sources.list.d/
```

Then run `apt update` as usual.

## Discussion

This discussion on [Reddit] provides some context:

> > >
> > > _-khumba- 2023-04-04T13:58:50Z_
> > >
> > > I'm not sure if I am allowed to post this here, if not, mods please
> > > remove.
> > >
> > > For anyone looking for prebuilt packages, I am providing just the
> > > essentials in an APT repository at <https://khumba.net/debian/>,
> > > system76-driver and the DKMS modules. The packaging has had minimal
> > > changes made to it to make it work with Debian.
> > >
> > > 100% unofficial, works for my darp6 and YMMV, but bug reports are
> > > welcome.
> > >
> >
> > _Philip-McGrath 2023-05-18T08:47:04Z_
> >
> > Thanks for sharing this! I'm in the process of moving my Thelio Mira from
> > Kubuntu 22.10 to Debian Bookworm, and I tried your packages just
> > now. Unfortunately, I'm having the same problem as with my previous
> > attempt (basically building the packages from the PPA with `apt source
> > --build`): the fan is always spinning at full blast on Debian, whereas on
> > Kubuntu it's basically silent.
> >
> > I'm actually a bit unclear which packages/git repos are specifically
> > relevant to the fan. A lot of them seem to deal with laptop stuff or a few
> > X quirks (I use Wayland).
> >
> > Anyway, I'd be glad to help get Debian-on-Thelio working.
> >
>
> _Philip-McGrath 2023-05-18T15:43:12Z_
>
> Success!
>
> [An old comment] pointed me toward `system76-power` for fan control, and it
> wasn't packaged at <https://khumba.net/debian/>. I removed and purged those
> packages and re-enabled the repository I'd cobbled together from the PPA
> with `apt source --build`. Then I did `sudo apt install
> --no-install-recommends system76-driver`, and the fan calmed down even
> before `apt` had finished printing its output!
>
> I'm not sure what the problem was last night. One possibility is that I'd
> used `--no-install-recommends` to avoid `hidpi-daemon` (I just use Wayland),
> and I'd later installed `lm-sensors` manually, but maybe it was actually
> needed and the order made a difference. Or maybe it was something else
> entirely.
>
> I'll figure out later somewhere reasonable to upload the packages I built.
> They are definitely a hack, but maybe they'd be useful for others.
>
> As I said, I'm willing to put some time into helping to get The Right Thing
> working: I'm hoping to stick with Bookworm on this machine for the next
> couple years. But for now, at least the fan won't be blasting while I
> migrate the system.
>

## License

My contributions to this repository may be used under either the [Apache-2.0]
license or the [MIT] (Expat) license, at your option. The packages themselves
are obviously under their own licenses, often from the GPL family. The sources
as retrieved and unpacked by `apt build --source` are included under the
`build-artifacts/` directory.

[PPA]: https://launchpad.net/~system76-dev/+archive/ubuntu/stable
[build instructions]: https://wiki.debian.org/CreatePackageFromPPA
[`aptly`]: https://packages.ubuntu.com/kinetic-updates/aptly
[repository guidelines]: https://wiki.debian.org/DebianRepository/UseThirdParty
[Reddit]: https://www.reddit.com/r/System76/comments/122lyii/is_there_a_way_to_get_system76_drivers_on_debian/jkm14at/?context=3
[An old comment]: https://www.reddit.com/r/System76/comments/rjed2m/comment/hp6ctc3/
[Apache-2.0]: https://spdx.org/licenses/MIT.html
[MIT]: https://spdx.org/licenses/MIT.html
