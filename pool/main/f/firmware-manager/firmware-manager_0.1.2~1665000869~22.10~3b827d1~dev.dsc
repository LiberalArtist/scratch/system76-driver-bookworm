-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: firmware-manager
Binary: firmware-manager, firmware-manager-notify, firmware-manager-shared, libfirmware-manager, libfirmware-manager-dev
Architecture: amd64 arm64 all
Version: 0.1.2~1665000869~22.10~3b827d1~dev
Maintainer: System76 <info@system76.com>
Homepage: https://github.com/pop-os/firmware-manager
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 9), cargo, rustc (>= 1.35.0), libdbus-1-dev, libgtk-3-dev, liblzma-dev, libssl-dev, libudev-dev, pkg-config
Package-List:
 firmware-manager deb admin optional arch=amd64,arm64
 firmware-manager-notify deb admin optional arch=amd64,arm64
 firmware-manager-shared deb admin optional arch=all
 libfirmware-manager deb admin optional arch=amd64,arm64
 libfirmware-manager-dev deb admin optional arch=all
Checksums-Sha1:
 5db2357c6edeff4748ac3e433f5e4a5c8356e8d6 41040296 firmware-manager_0.1.2~1665000869~22.10~3b827d1~dev.tar.xz
Checksums-Sha256:
 e55efb0b46c7e6cd470e53202c2c4bd0fb6af6f4b032e1a70ae54cf460685c38 41040296 firmware-manager_0.1.2~1665000869~22.10~3b827d1~dev.tar.xz
Files:
 4cadf9c9a0cf43f7f18aec7b26d3d931 41040296 firmware-manager_0.1.2~1665000869~22.10~3b827d1~dev.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJGBAEBCgAwFiEEY8Rt8BQNc4lhQp9OIE3YrsM6ev8FAmNtKxASHGluZm9Ac3lz
dGVtNzYuY29tAAoJECBN2K7DOnr/uAcQAMmv5wda+7JnYlCgQqyL4zBsm7iEZQce
OodQrjXZaCGf03rFG3wQWuIWDDT6DaokvGh0YzEdNQRpGAMdjKxw+d7xew6G3rLY
Y6KgnXF+gJtNrub6248Xft74W6q/n+TXQXwhNxL9PybcHvGgr/15JMqZGv54DYyS
PejJeDbs/3ogMkGQIINTFJer8610h1lHoOiHsQg+V+y0cwHqtjWftUuwdStNpqiu
1/KBdvG6i0Wi4QWE+1Hk/FMXMgFGGU8G08LAxVAR1sHSftg69TqIhJhtWZ3Ryens
xRTgascSWtfCB53TOW/oU94r+KQiVgSy49zqL8iLe/+/Sm0tBxd2/UKSPuF3p34/
0TjVNVmd3RueBg+SA6kiAr2HujCliZ/xniiumKBTKgFaAscUigv/wvhepGYbsOJs
9JC855pV27jseApIsHnlr7XDeOJo7zvQdhqMfCbDV3A2DZbU1jxT7tyX3yjwWKjL
iD0MQ0Q7SNJrIIQLXHlDNk+4i9Zu0KPsLfhVM2UGeSU3VBBJW21a370YrgLWbkm5
C0emo50p2+nQCDe37TsBWXlUBGhYEIC9h/0D6bv452i+GmqTYsYf73HpMSxqgQXV
i2v0RcwB0L6AHMeW3ELUCacTZjy946lPtGFTJyPO5UoPd9nzwNB0C5+ltQwYwNjy
KEuDWewccQIb
=9ymU
-----END PGP SIGNATURE-----
