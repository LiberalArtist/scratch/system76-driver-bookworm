-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: hidpi-daemon
Binary: hidpi-daemon
Architecture: all
Version: 18.04.6~1661971145~23.04~0ea00f5~dev
Maintainer: Aaron Honeycutt <aaron@system76.com>
Homepage: https://github.com/pop-os/hidpi-daemon
Standards-Version: 4.5.0
Build-Depends: debhelper (>= 9.20160709), dh-python, python3-all, pyflakes3, python3-gi, python3-pydbus, python3-xlib, gir1.2-notify-0.7
Package-List:
 hidpi-daemon deb x11 optional arch=all
Checksums-Sha1:
 801cddda49d18cd039eb6f18f4b32f1a808a9759 32024 hidpi-daemon_18.04.6~1661971145~23.04~0ea00f5~dev.tar.xz
Checksums-Sha256:
 30df52a824f8f8d459e3381403e4cd60cdf84caf08854c4618902048a93f1622 32024 hidpi-daemon_18.04.6~1661971145~23.04~0ea00f5~dev.tar.xz
Files:
 cd542116cea6246000ced7263552290b 32024 hidpi-daemon_18.04.6~1661971145~23.04~0ea00f5~dev.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJGBAEBCgAwFiEEY8Rt8BQNc4lhQp9OIE3YrsM6ev8FAmRJrkASHGluZm9Ac3lz
dGVtNzYuY29tAAoJECBN2K7DOnr/ElgQAMW05yAve3acVmwbG1yDR1i6CS4L2KXo
cAsfA3A1KC6VLedSM5A/YRuV/soo3OQcSwWhHPM4FfuHo7M2BX/yz7mF7Pyr5DrT
ysYw7enjc1fvm7TOYJJQbgski7o8Z5Y0wqb38QnKbBHnwGxgfZGwN4bb8AAnG/EX
+KI9idnv4W4+/9QTjftK0Qf/MyXhspuRbt/NDF8UY53nlWP8Wf5F4agG7Q5UhDiq
c76uGrF/OQ1K+N7zOkHG76ldPHbUS7tYvn7sxNjG2T/A9NJuy+RPHvXj6DZBexWu
wl4znQrbAzfRIAR5DMRfqm7eT8vENSHog5tnGPhOizOGF/oAR47pM3s5VBBwlxrV
iUqqJfu5oqNWdZFPgLyUu3DPgIX6nG00inGzJlkqXgKfg07XZLYM2x5kncRL/7fb
GhPzy1gTH6V2XR+U3CeRZavhRfjmDpklAwp6qpVHJuCwELjElXElZh1blPjGtWxc
GG5RP5wv1HkPhwFQft6fyOLPO64zsxwGheGKZoGS4WMo0DuHLUPmBp1zq20s02Kh
BqHrsZDod7xwATn4lot3MSTuDpHRb2nZQDcfEs4a6MW+OwoH39dD5WOHyE0v6Tu6
mpXG+I+BngcgbO/E3U40zI46jVeOKUdKkNy7QwVyZy3DgQDaoZ/ThIQiKuhYOfY4
vOQWexv3Y9tI
=Ey7S
-----END PGP SIGNATURE-----
