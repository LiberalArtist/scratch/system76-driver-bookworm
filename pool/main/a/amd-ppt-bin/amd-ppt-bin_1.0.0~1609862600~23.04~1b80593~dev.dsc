-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: amd-ppt-bin
Binary: amd-ppt-bin
Architecture: amd64
Version: 1.0.0~1609862600~23.04~1b80593~dev
Maintainer: Jeremy Soller <jeremy@system76.com>
Homepage: https://github.com/pop-os/amd-ppt-bin
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 10)
Package-List:
 amd-ppt-bin deb utils optional arch=amd64
Checksums-Sha1:
 8b7a797667351b89212ee7cb9a6ebe3da77ec751 104664 amd-ppt-bin_1.0.0~1609862600~23.04~1b80593~dev.tar.xz
Checksums-Sha256:
 1b22e48765d534fe3fa6f631e1cd706e53924f745e529e3818e1693d9421aa3f 104664 amd-ppt-bin_1.0.0~1609862600~23.04~1b80593~dev.tar.xz
Files:
 8006ecde14d538952d268355f39cc6f7 104664 amd-ppt-bin_1.0.0~1609862600~23.04~1b80593~dev.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJGBAEBCgAwFiEEY8Rt8BQNc4lhQp9OIE3YrsM6ev8FAmRJrXgSHGluZm9Ac3lz
dGVtNzYuY29tAAoJECBN2K7DOnr/1SwP/2AunmnQwpRtiGFnIZqnT6oW1UuV480v
gjWmgL8E3g6H4cONuIpDc9eq5RrY9rPDn73S0r3/K0KTLi/6K0mOxcS1IuBIWKG2
CYElEhESdE5F6SNSEfJAdP1neSB+9T37BGuzlgtQJDfoSgc1hEfPKM+x3TrOk4yZ
bEnRRNqHa65mQ2xnD7g8ThZ3Zio+nEx2Mto6W/kIZP0FDGy2rbMc30dK4uf+x0fP
nUfAWnUmc5Md9wOSr3seAjH7nEgheI3pOOU0EU2pAfbdOpTALpb2UsIak9kikUc+
6TTEJrwAJAl5VESdfGudo6i6c4RL7nOvS52aX0SH2+W135YNbj+0rFEZ+XwgRn8u
MdqWUcMFkdJs1tfT1en83NTH+kTyKJWK65aM+DJ8JE9CEeEk7oGERTLMKzE3me/J
mbKSGhEVOu5LInLgZ9WIQjxsGLLrWJ6+sVZoH0EWu0utAiwVwaT8yvHEExFAF0sK
MiUjvaIJA8RZP7NhpopJuwzWVnH6mNBOYkdvARyFAmRJoyofSlpYzGz+ckxaOdKc
jJxp+caFb/Vg4ly/lmBtrrB1aEDZUAcrq+jYHZPeKvUiRvg6sGyKVpWoji0D5YKK
DgLxXyUWlr1hmkJzO89tklYRoKcK6sj+YnXOg52kksO13ObGjHCCny5olTnRiae8
ASRjtD2SLDmZ
=QFnY
-----END PGP SIGNATURE-----
