-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: system76-acpi-dkms
Binary: system76-acpi-dkms
Architecture: amd64
Version: 1.0.2~1683055447~23.04~e7453c6~dev
Maintainer: Jeremy Soller <jeremy@system76.com>
Homepage: https://github.com/pop-os/system76-acpi-dkms
Standards-Version: 4.1.1
Build-Depends: debhelper (>= 9), dh-dkms, dkms
Package-List:
 system76-acpi-dkms deb kernel optional arch=amd64
Checksums-Sha1:
 fc178704d648cc05f63bd5141d0d3b154e560ac4 12600 system76-acpi-dkms_1.0.2~1683055447~23.04~e7453c6~dev.tar.xz
Checksums-Sha256:
 b97ec492655b0719bf8b9dcf1b9309b0a24a6752c240648fcda364b0913b0f23 12600 system76-acpi-dkms_1.0.2~1683055447~23.04~e7453c6~dev.tar.xz
Files:
 9d9f4a32aad6c0dd43e286415db0461e 12600 system76-acpi-dkms_1.0.2~1683055447~23.04~e7453c6~dev.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJGBAEBCgAwFiEEY8Rt8BQNc4lhQp9OIE3YrsM6ev8FAmRRZUISHGluZm9Ac3lz
dGVtNzYuY29tAAoJECBN2K7DOnr/7lkP/jRdkoOARpFI5xD2g6gPjUvCNx1sTEpd
tzXOBYkkyRc23MwUfiZ49eRxKqrIFckX5SK26W1NeibfnAWWaOc1HJ5ujuhW+lXC
wXrgfPdiJyuynsTnGBa08JfbD+wkJkrMApoKTYImBefrDzD7nwr0B+dFM4v0NXjb
pbog8WPUjvm4tcsgEOOsUZiecB3HYTtUFEEdLOI/i14QzU4j7aqEVHlbMrl+gg2E
ZjBQitHAiWuloLl16QiPzunM3KVyavvjPDVlrPrg9ap0ExiKLzkq+kSSucy6ndWg
8tc7cQIXGM1uKK9n/ciQEoCSSgyx26GpTPCwnIxEvnvIn7BkV3R3fo5VXxLHRPfq
b50EfVuRO+zWPnybZfw8ICOQEELpfaqsQ8EDyESjXA5LFsD29d1C18le3i7pWpsz
3/rD11VAkQS0raSgyXEaL8FQ5fbkZd/X5bbGzW/LoygZ9J91S6ifMga95UGis5Xc
/TdHxCMppduHL7vZKyNKE5rNVCaJiz5sW7nn4LPR31+RBsSgTwhlJGta0vkDOMVb
LGy8BsgV/kDXRidft3XvQxBRpcjocdmwURLBcTwojHUxq+wmAGAlY8JxaJa7ibUO
HfKq6o2o3zm8xjg8ttYZKhuH3Deqf71y0sFuf1Yqd5CMkftU++B8HivsGTzpHWuc
8uDDlf6nwtRO
=NCt9
-----END PGP SIGNATURE-----
