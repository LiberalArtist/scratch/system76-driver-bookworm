-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: system76-wallpapers
Binary: system76-wallpapers
Architecture: all
Version: 18.04.2~1611351210~23.04~52abc1e~dev
Maintainer: System76, Inc. <dev@system76.com>
Homepage: https://github.com/pop-os/system76-wallpapers
Standards-Version: 3.9.7
Build-Depends: debhelper (>= 9)
Package-List:
 system76-wallpapers deb x11 optional arch=all
Checksums-Sha1:
 329274b434e5530e2c82da5337152b1eb62fcd95 12822604 system76-wallpapers_18.04.2~1611351210~23.04~52abc1e~dev.tar.xz
Checksums-Sha256:
 91b78608a9769fae1d3062805cd73288629b04bf94ec64ba8e4acb35a63591d5 12822604 system76-wallpapers_18.04.2~1611351210~23.04~52abc1e~dev.tar.xz
Files:
 781f34bfebf6f3d2947a13c821eec12d 12822604 system76-wallpapers_18.04.2~1611351210~23.04~52abc1e~dev.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJGBAEBCgAwFiEEY8Rt8BQNc4lhQp9OIE3YrsM6ev8FAmRJrx8SHGluZm9Ac3lz
dGVtNzYuY29tAAoJECBN2K7DOnr/Ou0P/0WKgU+YOAVr6oCVMuu7is5KIduErC3J
SfzgZceCYWoUz77xm7ERuu2ppP/emTeoWjdZcgxOR0tfd9fLZgg/f3HQHa7JujJO
Z+abRErpv2ioJh2mDvqJItk8FEn6wH9NbpvkhIQ3btbJ3pFlH7uStXtBNrbss7jD
EfcD6pmImCkv/7WaYQ1ilKoQeBFbWvYI8AR3wNEdaAyPMHsrTg+r52+1+wrr3vKl
ov/okj2zPTJWaHx5B8s/wM1niLgrkJG6frksI2rxRaMeBEWGeH4zDAosROsnrGNZ
2gSGOuSM+7ehGBU3KTMkudrtQM5zcHJbsnL3Ek/uUQXD13UB2hoeNXy7gXO5vHGx
3JMoqBmFTlYN9Y1MTff+VgxSWzo4OL4faSTSCVk/+izfMMpNGwcY0l/8fijONV4n
uGN9/VCkjxcyhPOXGY6/nMn+d1mNCPqQk5/KSp66mu1WIxhX20d60Iv4Cx14cD2D
R0Wf3xHSlUtqwtBCtuVaewzhBfhC87yu8W7yWHUX2QcB2CWXbk33cNyzQofTeNkJ
s4os7UCALQdCmZG/bCali4Cw+8bUO9Ka6wfnXRZ8iQLxnCgHtEx1zqld5q8z9Uwp
CZFxj7BN6XhDD44obSByanARffUNZcOnpBf/jnsZwTZWJJcHCm8FBmDsqBr6vi+n
/Sr4GjU+IDZj
=FIrx
-----END PGP SIGNATURE-----
