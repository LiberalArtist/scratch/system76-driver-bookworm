-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: system76-driver
Binary: system76-driver, system76-driver-nvidia
Architecture: all
Version: 20.04.78~1683064889~23.04~5a4d74e~dev
Maintainer: System76, Inc. <dev@system76.com>
Homepage: https://launchpad.net/system76-driver
Standards-Version: 4.5.0
Build-Depends: debhelper (>= 9.20160709), dh-python, gir1.2-gtk-3.0, gir1.2-notify-0.7, pyflakes3, python3-all (>= 3.6), python3-dbus, python3-evdev, python3-gi, python3-distro, python3-systemd, xbacklight
Package-List:
 system76-driver deb utils extra arch=all
 system76-driver-nvidia deb utils extra arch=all
Checksums-Sha1:
 4c7d7bba56b5b2692d29f53c827aa3dace49bd05 95632 system76-driver_20.04.78~1683064889~23.04~5a4d74e~dev.tar.xz
Checksums-Sha256:
 4c2c1519a46785d8cfad755ab12673093b0be90df7dda76f55935b686b736fc7 95632 system76-driver_20.04.78~1683064889~23.04~5a4d74e~dev.tar.xz
Files:
 508b126bcb30dbcbd4027bca8b8fb301 95632 system76-driver_20.04.78~1683064889~23.04~5a4d74e~dev.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJGBAEBCgAwFiEEY8Rt8BQNc4lhQp9OIE3YrsM6ev8FAmRRiosSHGluZm9Ac3lz
dGVtNzYuY29tAAoJECBN2K7DOnr/0mAP/3jpkLoQba8+jIx4loWi36jsp7oPOAxt
YX747j/PPDg+LFt1Xk8F9eA7IfAVJqWDyW6bbJ6MAxaeE40tn6Kn0XMX/e/TizbA
JDATisxOWkKySuZczcDwQvjibnvQXfbBzJxHJhcIobUb1ngJev+Sihs3wjB2Iz+p
GtjCbe1aASaFHCpEbfF8cDdW3pVpamitxfGyEtnDn78+jdhmLTHK16kBJvCqTd/E
uXStyes3oNIgGsAITbP5DEuPSdgWOHTmcgOKByV0NpSvUVg18vy0qU5pML1e+HWP
o9yIvQT2jlYK1gLGcEFP/qs3TPpBEHKC+Omplp61n2SrNkzRcI3syuGD1vlA6rnB
KvXNRRRiLGnMc1UwVhopM5B8qwx5EmmR65OXUzx6hGVogjAwbcXOwmkIasHg4C3S
hr7aDvBK2z3O7gXd03EFJAEftIhDBQ/B6y3B/0jLcJ1RqPsZETMxWkaUGUdY/7zp
6gR6P5pnVyBQ3g+giGkaJpXfvepTFqrsCBn1+A8YZm4/yBqtxSn564XY8l5ldwbQ
cczFgV1QN7QqMXRDqruzmXxiATifRSsaEQGtG6WdqMDoRbpk/67+yqnbq4xBVsJK
IQdlbduteBJH7KWOYnFtH8xgSuTh3X7l5q7AAJR3OITWYpEIE3ssFET6QlPQEbjA
0n/HsYXLkLro
=KEP0
-----END PGP SIGNATURE-----
