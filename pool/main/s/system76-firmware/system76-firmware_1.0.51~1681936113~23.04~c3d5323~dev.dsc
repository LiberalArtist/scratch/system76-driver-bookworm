-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: system76-firmware
Binary: system76-firmware, system76-firmware-daemon
Architecture: amd64
Version: 1.0.51~1681936113~23.04~c3d5323~dev
Maintainer: Jeremy Soller <jeremy@system76.com>
Homepage: https://github.com/pop-os/system76-firmware
Standards-Version: 4.1.1
Build-Depends: debhelper (>= 9.20160709), ca-certificates, cargo, libdbus-1-dev, liblzma-dev, libssl-dev, pkg-config
Package-List:
 system76-firmware deb admin optional arch=amd64
 system76-firmware-daemon deb admin optional arch=amd64
Checksums-Sha1:
 8e2f371cc1579ceb210ae0c824c4bc7c06fd8c56 19511868 system76-firmware_1.0.51~1681936113~23.04~c3d5323~dev.tar.xz
Checksums-Sha256:
 421d7e1c8ac7482ee0b293863bff235addf76822c33773e7d08ffc43b96937d7 19511868 system76-firmware_1.0.51~1681936113~23.04~c3d5323~dev.tar.xz
Files:
 3213c1ef94e5bfb92610e6c9f2d6b51b 19511868 system76-firmware_1.0.51~1681936113~23.04~c3d5323~dev.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJGBAEBCgAwFiEEY8Rt8BQNc4lhQp9OIE3YrsM6ev8FAmRJrqkSHGluZm9Ac3lz
dGVtNzYuY29tAAoJECBN2K7DOnr/twsP+QEtfJAhtDmDIF58mnCLIRwC+gNTNmsp
U/Qq7ZQb2mptUrUZCxNdqL5QRmvZoYp9PwZgQ4EeZWWbTXMotO4BVQFaIOcIZidt
JoMK8JW6RPn69DSUXNYLqHSgfFNoixXtJMCHv0/TGeYtLXP9Cgi49u4Mupp7fc8n
x1izXs98qxuliTM0tFRR+roKiYAl27R3pOxrRkxiT/+HpvyCfiUThGuWNf005qRx
n2RLWmqIaIc6eALKpyNUa3tnQvWvUG8PxUodah5DZmfRhKKbsTXyfxK/Ls6aVAbK
1UQ2qmyTwC/cyBMRjOhIGGzdeSyPhkMwm+aLyx+9V2wj4g1pLWVM7vQc8vVXpWdE
Accy4IecL2NuiAibZX/37uAmIqYX495vWC07F/P47Ahhk4Tu5XIj2wKrYMbCfzFv
KnGrbxEQdtcnR6lHBfyl4gpHqgd80lnTxXDyEprnXZCN9m4sZBHmHeCkIorjcHVj
SuifefZ0jn7DIn3qb+KZxKEnOGXcxaur9OMcKCVuwPgERF5BczorkuspCs9THWQ9
h//8lZEf3xts6WdUQJUvTXG9sWG9RS/JfimlsmHo6oSu5fFIycy0uNcj0GV549nC
zw3PzfnYZLhkydwmzLQqgkoBNBcdEuQMWiifGtcB0Y+FG+utiGcH4ekL9DkLfQW+
F/3HR44dhXAk
=weIa
-----END PGP SIGNATURE-----
