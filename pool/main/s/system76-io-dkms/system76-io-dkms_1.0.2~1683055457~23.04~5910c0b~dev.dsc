-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: system76-io-dkms
Binary: system76-io-dkms
Architecture: amd64
Version: 1.0.2~1683055457~23.04~5910c0b~dev
Maintainer: Jeremy Soller <jeremy@system76.com>
Homepage: https://github.com/pop-os/system76-io-dkms
Standards-Version: 4.1.1
Build-Depends: debhelper (>= 9), dh-dkms, dkms
Package-List:
 system76-io-dkms deb kernel optional arch=amd64
Checksums-Sha1:
 79d9de180181176fd91017de8b45f83ab1ce8946 16868 system76-io-dkms_1.0.2~1683055457~23.04~5910c0b~dev.tar.xz
Checksums-Sha256:
 125f0b9f7949834766d737adb71c2dbefc96846d5d950e3c34cc14fdd0b56d95 16868 system76-io-dkms_1.0.2~1683055457~23.04~5910c0b~dev.tar.xz
Files:
 1c983f07ae7a813edb7e6aeaa76fcc82 16868 system76-io-dkms_1.0.2~1683055457~23.04~5910c0b~dev.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJGBAEBCgAwFiEEY8Rt8BQNc4lhQp9OIE3YrsM6ev8FAmRRZZUSHGluZm9Ac3lz
dGVtNzYuY29tAAoJECBN2K7DOnr/kFQQALQ94mB0TSaU9/oijeGtXM0xUdRyZd47
/Lst5X0htuOldeuKXhgo+tPfpLkKcgDZESeKzWQyKYNXGeMrx6XMrbZvDIjFKjDW
GQCLSO2EVTrQ2nUwq++yr2AiJl7SNhxN8X2mFjAcImfLxI2+bugiSi7wRQJ6vLEj
6RCuJNuVGR7WSuj7cdE96nqkaJOyjq3QRWvdoQkShMn3FQB5MDGpK2QUBjelUD6d
MKsttRZ0jE9P9azkl5VKJMEcZruRN/LA7LuUJVLuiO+GTOpikc8o+kGhFhMv5Wr2
dtXJXF3IHhWe/LWm21bLZWD93yMxMa9AxjIWpcOxjpYw9/tazAPydTITtwMqPdPA
/53F1KzKSaT5Tmmrp8/1wSVd4hva+P71+01vcDqXapzKmA8ebcIe6+2PPsh4Cfv3
KBzWBamdrQah/21HAKYUlW4jlghl9G5k/N9CxcI9/eFLdWNbykh6iezA1944cb4m
hnRXMKsPu+t8s/gMlrrFIhEqp/WEQLgig9qjbMLBTQfTQJMMEp9+mYuvWgxtkrw6
wr1HTW1QepI8V54QdvkgfWAWgItwiRAEK4rg09f9JhevihfEV+FjZWmey4/3+anF
Dq4N+eIbna43XkiK8MSkMysxkbgX5ilyBpB1U9IZBD8RGDRfrH/P/89sHZ4aenST
tIudUAsbfQVL
=oggO
-----END PGP SIGNATURE-----
