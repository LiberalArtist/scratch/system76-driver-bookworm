-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: system76-oled
Binary: system76-oled
Architecture: amd64
Version: 0.1.5~1664307225~23.04~98f778d~dev
Maintainer: Jeremy Soller <jeremy@system76.com>
Homepage: https://github.com/pop-os/system76-oled
Standards-Version: 4.1.1
Build-Depends: debhelper (>= 9), cargo, libdbus-1-dev, libx11-dev, libxrandr-dev, pkg-config
Package-List:
 system76-oled deb admin optional arch=amd64
Checksums-Sha1:
 f5a6c91c2287abfcbfd851cc0f3ffb62a32fea3d 5268020 system76-oled_0.1.5~1664307225~23.04~98f778d~dev.tar.xz
Checksums-Sha256:
 5e1e14fdd9ef70196dc9ec8c92a09ff42176c857a14a5d3ce4ac74415fc779de 5268020 system76-oled_0.1.5~1664307225~23.04~98f778d~dev.tar.xz
Files:
 75a619155ac1a0a4164dfa9f93b1fec5 5268020 system76-oled_0.1.5~1664307225~23.04~98f778d~dev.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJGBAEBCgAwFiEEY8Rt8BQNc4lhQp9OIE3YrsM6ev8FAmRJrtUSHGluZm9Ac3lz
dGVtNzYuY29tAAoJECBN2K7DOnr/4PgP/0ElXD4BwbIvghfqP1dGAedii+CNLfHg
n7HGKuPo7BBXEb4UWI6LaoBGqnka+nhFcJf85kfFCpjADvKuDuEUJsBDiA0WGZqU
HwP3nkhoJzUG3KI2bzcO2UgJsui6jrVQW1hzv7q+A+koUBk3c6Sv+tQDzuTTjDqB
5C526tR3bfwZFd++QvAotSifeI69MLne7HXI0xPJrTcoplqIBm9dnC74WMRwWwjj
UEyM0A4dU+gpewoOucBmVXRt1gxyibv7ggpWOz/jhKyEVwz87dKVzZjRDxaeFeyI
I9WhGUidovdXZwGxhv2yEH8eXJZ8zgPGZwX/9SfHnyVMvYrxnzJfx8VOT2JAff1b
2bV82K+XfsQjqrBM8Esqf6o+fz7DCPAKs5qrXLfExSxjqjpXk1m8Y9pHvjLEPH5b
XxLsCepEqZhbiRzOdvhdvrSKkutxK/XXh66mx2cnbbMXCKMi5Q+/R8lXDg8p2clP
xM7+dWLrzCFXK0uuPi8hDF2Mg4h6qCGy+EHQvOd1/h09HDolLps52VZpSxvOAh9D
Ze6rlWx3ETJ+OsFOYwxYOpgJkd01SF2wluPQiZiZim2vKs6c1xiUpYnphHMbXv7k
wqhGeHyZuXiXiqkhdM6dtZppsQkGitc25OpN64pa8So0VKzjnLfn8NrG+KVRnosX
fJW2VGMsXYmz
=kQq9
-----END PGP SIGNATURE-----
