-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: system76-power
Binary: system76-power
Architecture: amd64 arm64
Version: 1.1.24~1683064670~23.04~7fb637e~dev
Maintainer: Jeremy Soller <jeremy@system76.com>
Homepage: https://github.com/pop-os/system76-power
Standards-Version: 4.1.1
Build-Depends: debhelper-compat (= 11), cargo, libdbus-1-dev, libusb-1.0-0-dev, pkg-config
Package-List:
 system76-power deb admin optional arch=amd64,arm64
Checksums-Sha1:
 c0f0ee65d059ba44cd39f1af2b7553ccd3024f5c 12182084 system76-power_1.1.24~1683064670~23.04~7fb637e~dev.tar.xz
Checksums-Sha256:
 148c149f57ca5d12984d5717fb02dd25db773be0464cfde83d35e8f3a1cfee8f 12182084 system76-power_1.1.24~1683064670~23.04~7fb637e~dev.tar.xz
Files:
 40a0a55b768721043ebf965e39c8ba6e 12182084 system76-power_1.1.24~1683064670~23.04~7fb637e~dev.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJGBAEBCgAwFiEEY8Rt8BQNc4lhQp9OIE3YrsM6ev8FAmRRiygSHGluZm9Ac3lz
dGVtNzYuY29tAAoJECBN2K7DOnr/95IQAKTsF8lduGXGWSsiYawoL8HYfK6aTZ6t
2OAUn+0F6KqAJ75OtkV0PRtVe+QpLw9xaobR2mMwxH1ecS3HhgDJGUGVg6Zk9aOV
CzL6fiy8F8buivwdWuJsjxwIcQGwMHN/wPoSVWa+b828lk+4RZpoejAgBfGi1lx4
IFphrPTu1ZwqeMOOglpy/xHgCN0yS9rSYTYGPTjYKyTnb1pvoU/Gw/57+1XjPztj
zSFusMFHEvJlEhuyyW6JhLPHiQnRTqnsUq47UWY+rm8eZ4zVN6NCFpCdVo/qQ89G
TvvIWOvZwL7LLC+Yvb9O3HyfYTnxY7PStypAYjdw2n298GnEKiJhvezCl/XU7J9v
a0UxLUkXvZtr/JY3JHytJ0HRK2Zh5MiC2hkxl9JTPaknr2r/ckQ6ljhswQezRpW4
9dGLVAx1+ivmIvaRzhvaUTyAqE4EtkDL+ISgORtXPB+uPOHU5Q8CN+8vMB7BA8n/
ldCl0bdQ4yIi0f9sbN3XWm8RmkO2dEh56uAP530JVpDeo1Zvz3m9aZPgBxs/1eeQ
vGjLUWrN+fdDCiMIxD0oCS/9/8MfXGXWkr/doQHXzzJyMJP/A7ex8J0dflsfbwH1
VJA3+IogGMn1RAhIwpCqNg9al3kZk292762IRN114Qp/8FCt/KlI6AKOswZlXWp3
6DRxV4NOkFfB
=QDbe
-----END PGP SIGNATURE-----
