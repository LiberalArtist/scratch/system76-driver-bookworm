-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: system76-dkms
Binary: system76-dkms
Architecture: amd64
Version: 1.0.14~1683055453~23.04~53034d9~dev
Maintainer: Jeremy Soller <jeremy@system76.com>
Homepage: https://github.com/pop-os/system76-dkms
Standards-Version: 4.1.1
Build-Depends: debhelper (>= 9), dh-dkms, dkms
Package-List:
 system76-dkms deb kernel optional arch=amd64
Checksums-Sha1:
 4e297e120be42d3e45922f2eccc0959d6c396e22 16540 system76-dkms_1.0.14~1683055453~23.04~53034d9~dev.tar.xz
Checksums-Sha256:
 1a556c29dfe484ba09ed2ce36854c2c71edb417a82ee3c157192845bcf62e9c6 16540 system76-dkms_1.0.14~1683055453~23.04~53034d9~dev.tar.xz
Files:
 7ce090615ccc097b840a4573cfbec455 16540 system76-dkms_1.0.14~1683055453~23.04~53034d9~dev.tar.xz

-----BEGIN PGP SIGNATURE-----

iQJGBAEBCgAwFiEEY8Rt8BQNc4lhQp9OIE3YrsM6ev8FAmRRZYASHGluZm9Ac3lz
dGVtNzYuY29tAAoJECBN2K7DOnr/r3YP/A+UEssOO+vkGABTtWiIVP3BE4b95ZQ1
rjer6p+WD4fPWgOQa3a68Y/qoCqenOgZCvnY2XIcNeitKO/ZFcGLg7sbCyqFv8ZN
GlGMX4/Bp57ieNBtzksjdk5Vy5z4UaSElD11w+Qq6nVY15TuESrcuM5ScQ3fR+II
j3jJ+KQEC2HB4qJt2Vx9Zvmoik2YpMRxsIhsluZ4sAMHHRsImN4T8uH3GphTUfa5
uWUwDbmZ874WFJ9GLpyBqdosahWQWD7oiBMYJIcgRiY7jQVTVKhYFYUC0inNRSKk
um2b5BuMeTTpJp5Nx4sIfETksiU748Ffd4MmCH/2BtC0LcV03sdUxgpfKRwZEItk
M/H7o6CLhqalyeTFEobPfNgAd1ZQd7xjrXXJXiU9ax7hKWysH6NqKFw7O0PNXI5o
WowejusRUa/w6+wn1ROvMNGI5vn1gOUAzzsCp3LR7O8VUVtT00l2Q6OKRJcCwPuK
BXW+ZHxbu6J14s2o/UP3sQPC1xZg6fSnxoV09kzJmNZFQRzvOzZU0ol0B8667tVj
O/ciB7Hc3KWXBE5BUaFgmGUrln9tcJLZwGZZTI1yjdbz93VwjmfvelSVm7bxYN84
BLhBnhT9C/xp4o8thodoTUMWJ1/0Z/5cb/jOKWasjJ2yuz5KEyUhl6Y9ZjlM1iuz
H9pmjsjnuKbo
=utkK
-----END PGP SIGNATURE-----
