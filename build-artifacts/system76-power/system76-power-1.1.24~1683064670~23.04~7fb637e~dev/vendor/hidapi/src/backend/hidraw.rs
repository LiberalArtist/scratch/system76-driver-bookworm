use std::{
    ffi::{CString, OsStr, OsString},
    os::unix::prelude::OsStrExt,
    thread::current,
};

use crate::{DeviceInfo, HidError, HidResult, WcharString};

use super::{Backend, BackendHidDevice};
use udev::Enumerator;

struct HidrawBackend {
    device_list: Vec<DeviceInfo>,
    enumerator: Enumerator,
}

impl<'a> Backend<'a> for HidrawBackend {
    type DeviceListIterator = std::slice::Iter<'a, DeviceInfo>;

    type HidDevice = HidrawDevice;

    fn new() -> HidResult<Self> {
        let mut enumerator = Enumerator::new()?;
        enumerator.match_subsystem("hidraw")?;

        let backend = HidrawBackend {
            device_list: Vec::new(),
            enumerator,
        };

        Ok(backend)
    }

    fn refresh_devices(&mut self) -> crate::HidResult<()> {
        self.device_list.clear();

        let iter = self.enumerator.scan_devices()?.map(|dev| -> DeviceInfo {
            let mut info = DeviceInfo {
                // path: CString::new(dev.devpath().as_bytes()).unwrap(),
                // vendor_id: vid,
                // product_id: pid,
                // serial_number: serial
                //     .iter()
                //     .flat_map(|s| s.to_str().map(|s| WcharString::String(s.to_owned())))
                //     .next()
                //     .unwrap_or(WcharString::None),
                ..Default::default()
            };

            if let Some(usb_dev) = find_usb_device_node(&dev) {
                info.vendor_id = usb_dev
                    .property_value("ID_VENDOR_ID")
                    .map(|ostr| ostr.to_str().map(|str| u16::from_str_radix(str, 16).ok()))
                    .flatten()
                    .flatten()
                    .unwrap_or(0);

                info.product_id = usb_dev
                    .property_value("ID_MODEL_ID")
                    .map(|ostr| ostr.to_str().map(|str| u16::from_str_radix(str, 16).ok()))
                    .flatten()
                    .flatten()
                    .unwrap_or(0);

                info.serial_number = usb_dev
                    .property_value("ID_SERIAL")
                    .map(|s| s.to_owned())
                    .iter()
                    .flat_map(|s| s.to_str())
                    .next()
                    .map(|s| WcharString::String(s.to_owned()))
                    .unwrap_or(WcharString::None);
            }

            info.path = CString::new(dev.devpath().as_bytes()).unwrap();

            info
        });

        self.device_list.extend(iter);

        Ok(())
    }

    fn device_list(&'a self) -> Self::DeviceListIterator {
        self.device_list.iter()
    }

    fn open_with(device_info: &crate::DeviceInfo) -> crate::HidResult<Self::HidDevice> {
        todo!()
    }
}

fn find_usb_device_node(dev: &udev::Device) -> Option<udev::Device> {
    // First find usb_device node for extracting VID, PID, etc...
    let mut current_dev = dev.clone();
    loop {
        match current_dev.devtype() {
            Some(ty) if ty == "usb_device" => break,
            _ => (),
        }

        if let Some(parent) = current_dev.parent() {
            current_dev = parent;
        } else {
            return None;
        }
    }

    Some(current_dev)
}

struct HidrawDevice {}

impl BackendHidDevice for HidrawDevice {}

#[cfg(test)]
mod tests {
    use super::HidrawBackend;
    use crate::backend::Backend;

    #[test]
    fn enumeration() {
        let mut backend = HidrawBackend::new().unwrap();
        backend.refresh_devices().unwrap();

        for dev in backend.device_list() {
            println!("{:#?}", dev);
        }
    }
}
