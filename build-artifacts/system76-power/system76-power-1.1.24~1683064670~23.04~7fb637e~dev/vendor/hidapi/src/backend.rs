#[cfg(not(feature = "rust-linux-hidraw"))]
mod hidapi;
#[cfg(feature = "rust-linux-hidraw")]
mod hidraw;

use crate::{DeviceInfo, HidResult};

pub trait BackendHidDevice {}

pub trait Backend<'a>: Sized {
    type DeviceListIterator: Iterator<Item = &'a DeviceInfo>;
    type HidDevice: BackendHidDevice;

    fn new() -> HidResult<Self>;
    fn refresh_devices(&mut self) -> HidResult<()>;
    fn device_list(&'a self) -> Self::DeviceListIterator;
    fn open_with(device_info: &DeviceInfo) -> HidResult<Self::HidDevice>;
}
