extern crate hidapi;

use std::error::Error;

use hidapi::{HidApi, HidDevice};

fn main() -> Result<(), Box<dyn Error>> {
    let mut api = HidApi::new()?;
    api.refresh_devices().unwrap();

    let dev = api.open(0x046d, 0x0aaa).unwrap();

    std::thread::spawn(move || {
        let dev = api.open(0x046d, 0x0aaa).unwrap();
    }).join().unwrap();



    Ok(())
}
