# concat-in-place

[![Crates.io](https://img.shields.io/crates/v/concat-in-place)](https://crates.io/crates/concat-in-place) [![Docs.rs](https://docs.rs/concat-in-place/badge.svg)](https://docs.rs/concat-in-place)

Provides efficient concatenation of strings and vectors

The goal of these macros are to reduce the amount of allocations that are required
when concatenating string buffers and vectors; with a macro that makes it simple to
achieve in practice.

## Implementation Notes

- The caller must provide a buffer for appending the slices to
- The buffer is resized to accomodate the total length of all slices given

## String Concatenation

Appends any number of string slices onto a string buffer

```rust
use concat_in_place::strcat;

let domain = "domain.com";
let endpoint = "inventory/parts";
let id = "10512";

let mut url = String::new();
let slice = strcat!(&mut url, domain "/" endpoint "/" id);
assert_eq!(slice, "domain.com/inventory/parts/10512");
```

### Implementation Notes

Technically works with any string type that has the following methods:

- `capacity`
- `len`
- `push_str`

## Vector Concatenation

Appends any number of slices onto a vector

```rust
use concat_in_place::veccat;

let domain = b"domain.com";
let endpoint = b"inventory/parts";
let id = b"10512";

let mut url = Vec::new();
let slice = veccat!(&mut url, domain b"/" endpoint b"/" id);
assert_eq!(slice, b"domain.com/inventory/parts/10512");
```

### Implementation Notes

Technically works with any type that has the following methods:

- `capacity`
- `len`
- `reserve`
- `extend_from_slice`